FROM openjdk
WORKDIR /home
COPY target/angular-spring-3.0.0.jar  springboot.jar
ENTRYPOINT ["java","-jar","springboot.jar"]
